SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- user/group starting... ---------\n" >> ~/voxbox-install.log
addgroup --gid 1000 voxbox
adduser --uid 1000 --gid 1000 voxbox
adduser voxbox sudo
mkdir /home/voxbox/log/
printf "\n--------- user/group done ---------\n" >> ~/voxbox-install.log
