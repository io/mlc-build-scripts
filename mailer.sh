SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- mailer starting... ---------\n" >> ~/voxbox-install.log  
echo smtp.mandrillapp.com smtp --port=587 --starttls --user=$MANDRILL_ACCOUNT --pass=$MANDRILL_KEY > /etc/nullmailer/remotes
echo $HOSTNAME@$FQDN > /etc/nullmailer/defaultdomain
echo $ADMIN@$FQDN > /etc/nullmailer/adminaddr
echo MAILTO=\"$ADMIN@$FQDN\" >> /etc/crontab
echo $HOSTNAME@$FQDN > /etc/mailname
service nullmailer reload
printf "\n--------- mailer done ---------\n" >> ~/voxbox-install.log
