SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- bash starting... ---------\n" >> ~/voxbox-install.log
cat > ~/.bashrc <<'DELIM'
[ -z "$PS1" ] && return
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=1000
HISTFILESIZE=2000
shopt -s checkwinsize
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac
force_color_prompt=yes
if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        color_prompt=yes
    else
        color_prompt=
    fi
fi
PROMPT_COMMAND='history -a;echo -en "\033[m\033[38;5;2m"$(( $(sed -nu "s/MemFree:[\t ]\+\([0-9]\+\) kB/\1/p" /proc/meminfo)/1024))"\033[38;5;22m/"$(($(sed -nu "s/MemTotal:[\t ]\+\([0-9]\+\) kB/\1/Ip" /proc/meminfo)/1024 ))MB"\t\033[m\033[38;5;55m$(< /proc/loadavg)\033[m"'
PS1="\n\[\033[1;37m\]\342\224\214($(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]\h'; else echo '\[\033[01;35m\]\u@\h'; fi)\[\033[1;37m\])\342\224\200(\[\033[1;36m\]\$?\[\033[1;37m\])\342\224\200(\[\033[1;34m\]\@ \d\[\033[1;37m\])\[\033[1;37m\]\n\342\224\224\342\224\200(\[\033[1;32m\]\w\[\033[1;37m\])\342\224\200(\[\033[1;32m\]\$(ls -1 | wc -l | sed 's: ::g') files, \$(ls -lah | grep -m 1 total | sed 's/total //')b\[\033[1;37m\])\342\224\200> \[\033[0m\]"

unset color_prompt force_color_prompt
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
export rvmsudo_secure_path=0
DELIM
printf "\n--------- aliases ---------\n" >> ~/voxbox-install.log >> ~/voxbox-install.log
cat > ~/.bash_aliases <<DELIM

# Easy extract
extract () {
  if [ -f \$1 ] ; then
      case \$1 in
          *.tar.bz2)   tar xvjf \$1    ;;
          *.tar.gz)    tar xvzf \$1    ;;
          *.bz2)       bunzip2 \$1     ;;
          *.rar)       rar x \$1       ;;
          *.gz)        gunzip \$1      ;;
          *.tar)       tar xvf \$1     ;;
          *.tbz2)      tar xvjf \$1    ;;
          *.tgz)       tar xvzf \$1    ;;
          *.zip)       unzip \$1       ;;
          *.Z)         uncompress \$1  ;;
          *.7z)        7z x \$1        ;;
          *)           echo "don't know how to extract '\$1'..." ;;
      esac
  else
      echo "'\$1' is not a valid file!"
  fi
}

# Creates an archive from given directory
mktar() { tar cvf  "${1%%/}.tar"     "${1%%/}/"; }
mktgz() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }
mktbz() { tar cvjf "${1%%/}.tar.bz2" "${1%%/}/"; }

  alias ls='ls --color=auto' ## Colorize the ls output ##
  alias ll='ls -la' ## Use a long listing format ##
  alias l.='ls -d .* --color=auto' ## Show hidden files ##
  alias df='df -H'
  alias du='du -ch'
  alias top='atop'
  alias grep='grep --color=auto'
  alias mkdir='mkdir -pv'
  alias diff='colordiff'
  alias mount='mount |column -t'
  alias path='echo -e \${PATH//:\\n}'
  alias now='date +"%T"'
  alias nowtime=now
  alias nowdate='date +"%d-%m-%Y"'
  alias fastping='ping -c 100 -s.2'
  alias ports='netstat -tulanp'
  alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
  alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
  alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
  alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'
  alias firewall=iptlist
  alias update='sudo apt-get update && sudo apt-get upgrade'
  alias dnstop='dnstop -l 5 eth0'
  alias vnstat='vnstat -i eth0'
  alias iftop='iftop -i eth0'
  alias tcpdump='tcpdump -i eth0'
  alias ethtool='ethtool eth0'
  alias meminfo='free -m -l -t'
  alias psmem='ps auxf | sort -nr -k 4'
  alias psmem10='ps auxf | sort -nr -k 4 | head -10'
  alias pscpu='ps auxf | sort -nr -k 3'
  alias pscpu10='ps auxf | sort -nr -k 3 | head -10'
  alias cpuinfo='lscpu'
  alias wget='wget -c'
  alias sendmail='sendmail -f $ADMIN@$FQDN'

DELIM

. ~/.profile
printf "\n--------- bash done ---------\n" >> ~/voxbox-install.log
