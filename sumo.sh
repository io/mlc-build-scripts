SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
export START=$(date +%s.%N)

cd /src && mkdir sumo-logic && cd sumo-logic && wget http://git.voxbox.io/matteo/voxbox-config-files/raw/master/sumo-logic/sources.json
cd /etc && wget ${GIT_BUILD_SRC}/voxbox-config-files/raw/master/etc/sumo.conf
cd /src && wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}sumo_collector.sh && chmod +x sumo_collector.sh
sed -i "s/VAR_SUMO_ID/$SUMO_ID/g" /etc/sumo.conf
sed -i "s/VAR_SUMO_KEY/$SUMO_KEY/g" /etc/sumo.conf
./sumo_collector.sh -q -dir '/usr/local/SumoLogic'

export END=$(date +%s.%N)
sh $SCRIPTPATH/exec_time.sh