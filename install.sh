export GIT_BUILD_SRC=https://gitlab.com/io/
export GIT_PATH_SRC=mlc-build-scripts/raw/master/ 
export HOSTNAME=$(hostname)
export DEFAULT_SECRET=$(date +%s | sha256sum | base64 | head -c 32)
export BUILD_SCRIPTS_LIST=(
variables
packages
bash
mailer
databases
odbc
newrelic
nginx
rvm_ruby_rails
sipdump
swap
freeswitch
frontend
newrelic_plugins
fail2ban
monit
mysql_additional
ssh
reboot
)

printf "\n--------- install starting... ---------\n" >> ~/voxbox-install.log   
printf "\n--------- ${USER}@${HOSTNAME} at $(date)---------\n" >> ~/voxbox-install.log 
date >> ~/voxbox-install.log

echo $DEFAULT_SECRET > default_secret.txt
mkdir /src && mkdir /home/voxbox/log && mkdir /tmp/voxbox && cd /tmp/voxbox
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}variables.sh && wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}exec_time.sh

for NAME in "${BUILD_SCRIPTS_LIST[@]}"
do
        wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}${NAME}.sh
        chmod +x ${NAME}.sh
        ./${NAME}.sh
done


 
