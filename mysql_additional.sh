SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
      
export GIT_PATH_SRC=mlc-config-files/raw/master/  

printf "\n--------- MySQL Additional Settings starting... ---------\n" >> ~/voxbox-install.log

#additional SQL script
cd /usr/src
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}mysql/fnGetCustomerRate.sql
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}mysql/fnGetProviderRate.sql
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}mysql/fnIPAddress2RouteID.sql
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}mysql/fnNumber2Destination.sql
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}mysql/fnNumber2DestinationID.sql
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}mysql/fnNumber2TechPrefix.sql
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}mysql/active_calls_view.sql


# sed failed at this point!
perl -e "s/VAR_HOSTNAME/$HOSTNAME/g;" -pi.save $(find /usr/src/ -type f)
rm -rf *.save

/usr/bin/mysql -u root -p${DEFAULT_SECRET} voxbox-backend < fnGetCustomerRate.sql
/usr/bin/mysql -u root -p${DEFAULT_SECRET} voxbox-backend < fnGetProviderRate.sql
/usr/bin/mysql -u root -p${DEFAULT_SECRET} voxbox-backend < fnIPAddress2RouteID.sql
/usr/bin/mysql -u root -p${DEFAULT_SECRET} voxbox-backend < fnNumber2Destination.sql
/usr/bin/mysql -u root -p${DEFAULT_SECRET} voxbox-backend < fnNumber2DestinationID.sql
/usr/bin/mysql -u root -p${DEFAULT_SECRET} voxbox-backend < fnNumber2TechPrefix.sql
/usr/bin/mysql -u root -p${DEFAULT_SECRET} voxbox-backend < active_calls_view.sql

printf "\n--------- MySQL Additional Settings end... ---------\n" >> ~/voxbox-install.log



