SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- reboot starting... ---------\n" >> ~/voxbox-install.log
rm -Rf /tmp/voxbox-tmp
apt-get update && apt-get -y upgrade
date >> ~/voxbox-install.log
printf "\n--------- install done ---------\n" >> ~/voxbox-install.log
echo "root:$DEFAULT_SECRET" | chpasswd
init 6
