SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- newrelic starting... ---------\n" >> ~/voxbox-install.log
echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list
wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -
apt-get update && apt-get install -y newrelic-sysmond
nrsysmond-config --set license_key=$NEW_RELIC_KEY  
printf "\n--------- newrelic done ---------\n" >> ~/voxbox-install.log
