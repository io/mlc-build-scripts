SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
export START=$(date +%s.%N)
       
export GIT_CONFIG_REPO=mlc-config-files
export GIT_PATH_SRC=/mlc-config-files/raw/master/  

printf "\n--------- freeswitch starting... ---------\n" >> ~/voxbox-install.log

cat >> /etc/fstab  <<DELIM
# Tmpfs (default size)
tmpfs /opt/db tmpfs defaults 0 0
# To specify a size use:
# tmpfs /opt/db tmpfs defaults,size=4g 0 0
DELIM

cd /src && wget ${GIT_BUILD_SRC}${GIT_CONFIG_REPO}/repository/archive.tar.gz && tar -xvf archive.tar.gz
git clone http://git.voxbox.io/matteo/voxbox-freeswitch.git -b v${FS_VERSION} 
cp ${GIT_CONFIG_REPO}.git/freeswitch/modules.conf $FS_SRC_DIR/ 

printf "\n--------- building freeswitch... ---------\n" >> ~/voxbox-install.log
cd $FS_SRC_DIR && sh bootstrap.sh -j && ./configure --prefix=$FS_BASE_DIR
make && make all install cd-sounds-install cd-moh-install

printf "\n--------- freeswitch build done! ---------\n" >> ~/voxbox-install.log
ln -s $FS_BASE_DIR/bin/freeswitch /usr/bin && ln -s $FS_BASE_DIR/bin/fs_cli /usr/bin
adduser --disabled-password -q --system --home $FS_BASE_DIR --gecos "Voice Platform" --ingroup daemon freeswitch
adduser freeswitch audio && chmod -R o-rwx $FS_BASE_DIR && mkdir $FS_BASE_DIR/ramdisk
cd /etc/init.d && rm freeswitch
              

wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}etc/init.d/freeswitch && chmod +x /etc/init.d/freeswitch
update-rc.d -f freeswitch defaults && update-rc.d -f freeswitch enable
cat > /etc/default/freeswitch  <<DELIM
DAEMON_OPTS="-nonat -nc -rp"
DELIM
cp $FS_BASE_DIR/conf/freeswitch.serial $FS_BASE_DIR/
rm -Rf $FS_BASE_DIR/conf/* && cp -Rf /src/${GIT_CONFIG_REPO}.git/freeswitch/conf/* $FS_BASE_DIR/conf/
sed -i "s/VAR_FS_EXT_IP/$FS_EXT_IP/g" $FS_BASE_DIR/conf/settings.xml
sed -i "s/VAR_DEFAULT_SECRET/$DEFAULT_SECRET/g" $FS_BASE_DIR/conf/settings.xml
sed -i "s/VAR_DEFAULT_SECRET/$DEFAULT_SECRET/g" $FS_BASE_DIR/conf/mod/event_socket.conf.xml
rm /src/archive.tar.gz && rm -Rf /src/voxbox-config-files.git/ 
wget http://git.voxbox.io/matteo/voxbox-config-files/raw/master/etc/fs_cli.conf && mv fs_cli.conf /etc/
sed -i "s/VAR_DEFAULT_SECRET/$DEFAULT_SECRET/g" /etc/fs_cli.conf

cd /etc/cron.daily 
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}etc/cron.daily/freeswitch_log_rotation
chmod 755 /etc/cron.daily/freeswitch_log_rotation
sed -i 's/RepeatedMsgReduction\ on/RepeatedMsgReduction\ off/' /etc/rsyslog.conf


cd $FS_BASE_DIR/scripts
wget ${GIT_BUILD_SRC}voxbox-lua-billing/raw/master/voxbox_billing_v2.lua 
wget ${GIT_BUILD_SRC}voxbox-lua-billing/raw/master/voxbox_functions_v2.lua
wget ${GIT_BUILD_SRC}voxbox-lua-billing/raw/master/voxbox_variables.lua
sed -i "s/VAR_DEFAULT_SECRET/$DEFAULT_SECRET/g" $FS_BASE_DIR/scripts/voxbox_variables.lua

chmod -R +x $FS_BASE_DIR/bin/
chown -R freeswitch:daemon $FS_BASE_DIR 
service freeswitch start
export END=$(date +%s.%N)
sh $SCRIPTPATH/exec_time.sh 
