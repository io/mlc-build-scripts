SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

DIFF_SEC=$(echo "$END - $START" | bc)
DIFF_MIN=$(echo "$DIFF_SEC / 60" | bc)
DIFF_HOU=$(echo "$DIFF_SEC / 3600" | bc)
printf "\n--------- $SCRIPT executed in $DIFF_MIN min ($DIFF_SEC sec) ---------\n" >> ~/voxbox-install.log
