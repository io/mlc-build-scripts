SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- odbc starting... ---------\n" >> ~/voxbox-install.log
cd /etc
rm odbc.ini && wget http://git.voxbox.io/matteo/voxbox-config-files/raw/master/etc/odbc.ini
rm odbcinst.ini && wget http://git.voxbox.io/matteo/voxbox-config-files/raw/master/etc/odbcinst.ini
sed -i "s/VAR_DEFAULT_SECRET/$DEFAULT_SECRET/g" /etc/odbc.ini
sed -i "s/VAR_HOSTNAME/$HOSTNAME/g" /etc/odbc.ini
printf "\n--------- odbc done ---------\n" >> ~/voxbox-install.log
