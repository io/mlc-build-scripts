#!/bin/bash
SCRIPT=$(readlink -f "$0"); SCRIPTPATH=$(dirname "$SCRIPT"); . $SCRIPTPATH/variables.sh
export START=$(date +%s.%N); export SCRIPT_NAME="databases"
printf "\n--------- Installing MySQL Server ---------\n" >> ~/voxbox-install.log
apt-get install -y mysql-server
printf "\n--------- Setting Root Secret ---------\n" >> ~/voxbox-install.log
mysqladmin -u root password $DEFAULT_SECRET
printf "\n--------- Preparing MongoDB Installation ---------\n" >> ~/voxbox-install.log
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | tee /etc/apt/sources.list.d/mongodb.list
apt-get update
printf "\n--------- Installing MongoDB Server ---------\n" >> ~/voxbox-install.log
apt-get install -y mongodb-10gen
export END=$(date +%s.%N)
. $SCRIPTPATH/exec_time.sh
