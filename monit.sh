SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- monit starting... ---------\n" >> ~/voxbox-install.log
cat > /etc/monit/conf.d/fail2ban  <<DELIM
check process fail2ban with pidfile /var/run/fail2ban/fail2ban.pid
        group services
        start program = "/etc/init.d/fail2ban start"
        stop  program = "/etc/init.d/fail2ban stop"
        if 5 restarts within 5 cycles then timeout
DELIM

cat > /etc/monit/conf.d/nullmailer  <<DELIM
check process nullmailer with pidfile /var/run/nullmailer.pid
        group services
        start program = "/etc/init.d/nullmailer start"
        stop  program = "/etc/init.d/nullmailer stop"
        if 5 restarts within 5 cycles then timeout
DELIM

cat > /etc/monit/conf.d/newrelic  <<DELIM
check process nrsysmond with pidfile /var/run/nrsysmond.pid
        group services
        start program = "/etc/init.d/newrelic-sysmond start"
        stop  program = "/etc/init.d/newrelic-sysmond stop"
        if 5 restarts within 5 cycles then timeout
DELIM

cat > /etc/monit/conf.d/nginx  <<DELIM
check process nginx with pidfile /var/run/nginx.pid
        group services
        start program = "/etc/init.d/nginx start"
        stop  program = "/etc/init.d/nginx stop"
        if 5 restarts within 5 cycles then timeout
DELIM

cat > /etc/monit/conf.d/freeswitch  <<DELIM
set daemon 60
set logfile syslog facility log_daemon
 
check process freeswitch with pidfile /opt/run/freeswitch.pid
  start program = "/etc/init.d/freeswitch start"
  stop program = "/etc/init.d/freeswitch stop"
  if 5 restarts within 5 cycles then timeout
  if cpu > 60% for 2 cycles then alert
  if cpu > 80% for 5 cycles then alert
  if totalmem > 2000.0 MB for 5 cycles then restart
  if children > 2500 then restart
DELIM

cat > /etc/monit/conf.d/sipdump  <<DELIM
check process pcapsipdump
		matching "pcapsipdump"
        start program = "/etc/init.d/sipdump start"
        stop  program = "/etc/init.d/sipdump stop"
        if 5 restarts within 5 cycles then timeout
DELIM

service monit restart
printf "\n--------- monit done ---------\n" >> ~/voxbox-install.log

