SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
export START=$(date +%s.%N)
printf "\n--------- rvm, ruby & rails starting... ---------\n" >> ~/voxbox-install.log

gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3
curl -L https://get.rvm.io | bash -s stable --autolibs=enabled
. /etc/profile.d/rvm.sh && . ~/.profile
rvm install $RUBY_VERSION && rvm fix-permissions && rvm user gemsets && rvm gemset create $GEMSET
# Load RVM into a shell session *as a function*
if [[ -s "$HOME/.rvm/scripts/rvm" ]] ; then
  # First try to load from a user install
  source "$HOME/.rvm/scripts/rvm"
elif [[ -s "/usr/local/rvm/scripts/rvm" ]] ; then
  # Then try to load from a root install
  source "/usr/local/rvm/scripts/rvm"
else
  printf "ERROR: An RVM installation was not found.\n"  >> ~/voxbox-install.log
fi
rvm gemset use $GEMSET --default
gem install bundler --no-rdoc --no-ri && gem install rails --no-ri --no-rdoc
export END=$(date +%s.%N)
sh $SCRIPTPATH/exec_time.sh 
