SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- newrelic plugins starting... ---------\n" >> ~/voxbox-install.log
# Load RVM into a shell session *as a function*
if [[ -s "$HOME/.rvm/scripts/rvm" ]] ; then
  # First try to load from a user install
  source "$HOME/.rvm/scripts/rvm"
elif [[ -s "/usr/local/rvm/scripts/rvm" ]] ; then
  # Then try to load from a root install
  source "/usr/local/rvm/scripts/rvm"
else
  printf "ERROR: An RVM installation was not found.\n"  >> ~/voxbox-install.log
fi
rvm use $RUBY_VERSION 
cd /src 
git clone http://git.voxbox.io/matteo/voxbox-newrelic-plugins.git && cd voxbox-newrelic-plugins
cd newrelic-mongodb-agent
rvm gemset create newrelic-mongodb-agent && rvm gemset use newrelic-mongodb-agent
gem install bundler --no-rdoc --no-ri && bundle
gem install foreman --no-rdoc --no-ri
gem install foreman-export-initscript --no-rdoc --no-ri
cp config/template_newrelic_plugin.yml config/newrelic_plugin.yml
sed -i "s/VAR_NEW_RELIC_KEY/$NEW_RELIC_KEY/g" config/newrelic_plugin.yml
sed -i "s/VAR_HOSTNAME/$HOSTNAME/g" config/newrelic_plugin.yml
mkdir log && touch log/newrelic-mongodb-agent.log
foreman export initscript /etc/init.d/ -a newrelic-mongodb-agent -u root
chmod +x /etc/init.d/newrelic-mongodb-agent
update-rc.d newrelic-mongodb-agent defaults
service newrelic-mongodb-agent start

cd ../newrelic_nginx_agent
rvm gemset create newrelic-nginx-agent && rvm gemset use newrelic-nginx-agent
gem install bundler --no-rdoc --no-ri && bundle
gem install foreman --no-rdoc --no-ri
gem install foreman-export-initscript --no-rdoc --no-ri
cp config/newrelic_plugin.yml.in config/newrelic_plugin.yml
sed -i "s/VAR_NEW_RELIC_KEY/$NEW_RELIC_KEY/g" config/newrelic_plugin.yml
sed -i "s/VAR_HOSTNAME/$HOSTNAME/g" config/newrelic_plugin.yml
cat > Procfile <<DELIM
newrelic-nginx-agent: ./newrelic_nginx_agent start
DELIM
foreman export initscript /etc/init.d/ -a newrelic-nginx-agent -u root
chmod +x /etc/init.d/newrelic-nginx-agent 
update-rc.d newrelic-nginx-agent defaults
service newrelic-nginx-agent start 


cd
LICENSE_KEY=$NEW_RELIC_KEY UNATTENDED=true bash -c "$(curl -sSL https://download.newrelic.com/npi/release/install-npi-linux-debian-x64.sh)"
cd /root/newrelic-npi

#manual installation
./npi config set license_key $NEW_RELIC_KEY
echo yes | ./npi fetch com.newrelic.plugins.mysql.instance
echo no | ./npi prepare com.newrelic.plugins.mysql.instance
#echo yes |./npi start com.newrelic.plugins.mysql.instance --foreground
./npi add-service com.newrelic.plugins.mysql.instance --start

cd ~/newrelic-npi/plugins/com.newrelic.plugins.mysql.instance/newrelic_mysql_plugin-2.0.0/config

cat > plugin.json <<DELIM
{
  "agents": [
    {
      "name"    : "\$HOSTNAME",
      "host"    : "localhost",
      "metrics" : "status,newrelic",
      "user"    : "root",
      "passwd"  : "\$DEFAULT_SECRET"
    }
  ]
}

DELIM

/etc/init.d/newrelic_plugin_com.newrelic.plugins.mysql.instance stop
cd /etc/init.d/
mv newrelic_plugin_com.newrelic.plugins.mysql.instance newrelic-mysql-agent
/etc/init.d/newrelic-mysql-agent start
