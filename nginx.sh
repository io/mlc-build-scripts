SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- nginx starting... ---------\n" >> ~/voxbox-install.log
apt-get install -y nginx
cat > /etc/nginx/nginx.conf  <<DELIM

user www-data;
worker_processes 4;
pid /run/nginx.pid;

events {
  worker_connections 768;
  accept_mutex on;
  use epoll;
}

http {

  sendfile on;
  tcp_nopush on;
  tcp_nodelay off;
  keepalive_timeout 60; 
  include /etc/nginx/mime.types;
  default_type application/octet-stream;
  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;

  gzip on;
  gzip_disable \"MSIE [1-6]\.\";
  # gzip_proxied any;
  # gzip_min_length 500;
  # gzip_http_version 1.1;
  # gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

  include /etc/nginx/conf.d/*.conf;
  include /etc/nginx/sites-enabled/*;
}
DELIM
printf "\n--------- nginx defaults ---------\n" >> ~/voxbox-install.log
rm /etc/nginx/sites-enabled/default && rm /etc/nginx/sites-available/default

export $GIT_PATH_SRC=/mlc-config-files/raw/master/
cd /etc/nginx/sites-available && wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}etc/nginx/sites-available/voxbox-stub-status
sed -i "s/VAR_HOSTNAME/$HOSTNAME/g" /etc/nginx/sites-available/voxbox-stub-status
sed -i "s/VAR_FS_EXT_IP/$FS_EXT_IP/g" /etc/nginx/sites-available/voxbox-stub-status
cd /etc/nginx/sites-enabled && ln -sf /etc/nginx/sites-available/voxbox-stub-status

printf "\n--------- nginx done ---------\n" >> ~/voxbox-install.log
