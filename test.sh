#!/bin/bash
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- tests starting... ---------\n" >> ~/voxbox-install.log
SERVICE_LIST=(
mysql
mongo
nullmailer
newrelic
nginx
fail2ban
monit
pcapsipdump
freeswitch
unicorn
)
FOLDERS_LIST=(
/swapfile
/home/voxbox
/etc/cron.daily/freeswitch_log_rotation
)
	
sleep 60

for NAME in "${SERVICE_LIST[@]}"
do	
  CNT=$(ps aux | grep ${NAME} | grep -v grep | wc -l)
  if [ $CNT -gt 0 ]; then 
   printf "\n--------- ${NAME} OK ---------\n" >> ~/voxbox-install.log
  else
   printf "\n--------- ${NAME} NOT OK ---------\n" >> ~/voxbox-install.log	
  fi
done

for NAME in "${FOLDERS_LIST[@]}"
do	
  CNT=$(ls -l ${NAME} | wc -l)
  if [ $CNT -gt 0 ]; then
   printf "\n--------- ${NAME} OK ---------\n" >> ~/voxbox-install.log
  else
   printf "\n--------- ${NAME} NOT OK ---------\n" >> ~/voxbox-install.log	
  fi
done

printf "\n--------- tests done ---------\n" >> ~/voxbox-install.log

uuencode ~/voxbox-install.log ~/voxbox-install.log | sendmail ${ADMIN}@${FQDN}
