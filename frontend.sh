SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
export START=$(date +%s.%N)
export GIT_BUILD_SRC=http://git.voxbox.io/matteo/

printf "\n--------- frontend starting... ---------\n" >> ~/voxbox-install.log
# Load RVM into a shell session *as a function*
if [[ -s "$HOME/.rvm/scripts/rvm" ]] ; then
  # First try to load from a user install
  source "$HOME/.rvm/scripts/rvm"
elif [[ -s "/usr/local/rvm/scripts/rvm" ]] ; then
  # Then try to load from a root install
  source "/usr/local/rvm/scripts/rvm"
else
  printf "ERROR: An RVM installation was not found.\n"  >> ~/voxbox-install.log
fi
rvm gemset use $GEMSET
rvm alias create $GEMSET ruby-$RUBY_VERSION@$GEMSET

cd /srv && git clone $GIT_BUILD_SRC/voxbox-switch-frontend.git && mv voxbox-switch-frontend voxbox && cd voxbox 
sed -i "s/VAR_DEFAULT_SECRET/$DEFAULT_SECRET/g" config/application.yml
sed -i "s/VAR_HOSTNAME/$HOSTNAME/g" config/application.yml
npm install --save-dev bower
rake bower:update['--allow-root']
bundle install
#bundle exec rake voxbox:setup

rvm wrapper $GEMSET unicorn_rails
export GIT_PATH_SRC=mlc-config-files/raw/master/
cd /etc/nginx/sites-available && wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}etc/nginx/sites-available/$RAILS_APP_NAME
sed -i "s/VAR_FS_EXT_IP/$PUBLIC_IP/g" /etc/nginx/sites-available/$RAILS_APP_NAME
cd /etc/nginx/sites-enabled && ln -sf /etc/nginx/sites-available/$RAILS_APP_NAME
service nginx restart

cd /etc/init.d/ 
wget ${GIT_BUILD_SRC}${GIT_PATH_SRC}etc/init.d/unicorn 
chmod +x unicorn
update-rc.d -f unicorn defaults 
update-rc.d -f unicorn enable
export END=$(date +%s.%N)
sh $SCRIPTPATH/exec_time.sh 
