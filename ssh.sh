SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- ssh starting... ---------\n" >> ~/voxbox-install.log
sed -i "s/Port 22/Port 65022/g" /etc/ssh/sshd_config
sed -i "s/PermitRootLogin without-password/PermitRootLogin yes/g" /etc/ssh/sshd_config
printf "\n--------- ssh done ---------\n" >> ~/voxbox-install.log
