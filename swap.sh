SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- swap starting... ---------\n" >> ~/voxbox-install.log
dd if=/dev/zero of=/swapfile bs=1024 count=1050576
mkswap /swapfile
swapon /swapfile
echo /swapfile swap swap defaults 0 0 >> /etc/fstab
printf "\n--------- swap done ---------\n" >> ~/voxbox-install.log
