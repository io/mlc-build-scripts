export DEBIAN_FRONTEND=noninteractive
export GIT_BUILD_SRC=https://gitlab.com/io/
export PUBLIC_IP=`ifconfig|xargs|awk '{print $7}'|sed -e 's/[a-z]*:/''/'`
# common
export FQDN=voxbox.io
export ADMIN=matteo
# api keys
export NEW_RELIC_KEY=ef34d31f458e0df90fc5c6e9ad19cfadc01213ef     
export MANDRILL_KEY=2a63b2ea-1f60-4c48-a543-d3ebe296e0e5
export MANDRILL_ACCOUNT=matteo@voxbox.io
# rails
export RAILS_APP_NAME=voxbox
export RUBY_VERSION=2.1.2
export GEMSET=voxbox
#
export FS_BASE_DIR=/opt
export FS_SRC_DIR=/src/voxbox-freeswitch
export FS_EXT_IP=`ifconfig|xargs|awk '{print $7}'|sed -e 's/[a-z]*:/''/'`
export FS_VERSION=1.4