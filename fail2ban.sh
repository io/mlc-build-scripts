SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- fail2ban starting... ---------\n" >> ~/voxbox-install.log
cat > /etc/fail2ban/jail.conf  <<DELIM
# Fail2Ban configuration file.
#

[DEFAULT]
ignoreip = 127.0.0.1/8
bantime = 608400
maxretry = 5
backend = auto
destemail = $ADMIN@$FQDN
banaction = iptables-multiport
mta = sendmail
protocol = all
chain = INPUT

action_    = %(banaction)s[name=%(__name__)s, port=\"%(port)s\", protocol=\"%(protocol)s\", chain=\"%(chain)s\"]
action_mw  = %(banaction)s[name=%(__name__)s, port=\"%(port)s\", protocol=\"%(protocol)s\", chain=\"%(chain)s\"]
             %(mta)s-whois[name=%(__name__)s, dest=\"%(destemail)s\", protocol=\"%(protocol)s\", chain=\"%(chain)s\"]
action_mwl = %(banaction)s[name=%(__name__)s, port=\"%(port)s\", protocol=\"%(protocol)s\", chain=\"%(chain)s\"]
             %(mta)s-whois-lines[name=%(__name__)s, dest=\"%(destemail)s\", logpath=%(logpath)s, chain=\"%(chain)s\"]
action     = %(action_mw)s

[ssh]
enabled  = true
port     = 65022
filter   = sshd
logpath  = /var/log/auth.log
bantime  = 31536000

[ssh-ddos]
enabled  = true
port     = 65022
filter   = sshd-ddos
logpath  = /var/log/auth.log
bantime  = 31536000

[freeswitch-dos]
enabled = true
port = 5060,5061,5080,5081
protocol = udp
filter = freeswitch-dos
logpath = /home/voxbox/log/voxbox/voxbox.log
action = iptables-allports[name=freeswitch-dos, protocol=all]
         sendmail-whois[name=FreeSwitch (DOS)]
maxretry = 5
findtime = 30
bantime  = 31536000

[freeswitch-acl]
enabled = true
port = 5060,5061,5080,5081
protocol = udp
filter = freeswitch-acl
logpath = /home/voxbox/log/voxbox/voxbox.log
action = iptables-allports[name=freeswitch-dos, protocol=all]
         sendmail-whois[name=FreeSwitch (DOS)]
maxretry = 5
findtime = 30
bantime  = 31536000

[rails-app]
enabled = true
port = http,https
filter = rails-app
logpath = /srv/voxbox/log/fail2ban.log
bantime = 31536000
findtime = 600
maxretry = 10
DELIM

sed -i "s,sender = .\+,sender=$ADMIN@$FQDN," /etc/fail2ban/action.d/sendmail-whois.conf
sed -i "s,dest = .\+,dest=$ADMIN@$FQDN," /etc/fail2ban/action.d/sendmail-whois.conf 
#sed -i -e s,'<param name="log-auth-failures" value="false"/>','<param name="log-auth-failures" value="true"/>', /usr/local/freeswitch/conf/sip_profiles/internal.xml

cat > /etc/fail2ban/filter.d/freeswitch-dos.conf  <<DELIM

[Definition]

# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named \"host\". The tag \"<HOST>\" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>[\w\-.^_]+)
# Values:  TEXT
#
failregex = \[WARNING\] sofia_reg.c:\d+ SIP auth challenge \(REGISTER\) on sofia profile \'\w+\' for \[.*\] from ip <HOST>
            \[WARNING\] sofia_reg.c:\d+ SIP auth failure \(REGISTER\) on sofia profile \'\w+\' for \[.*\] from ip <HOST>
            \[WARNING\] sofia_reg.c:\d+ SIP auth failure \(INVITE\) on sofia profile \'\w+\' for \[.*\] from ip <HOST>
            \[WARNING\] sofia_reg.c:\d+ Can't find user \[.*\] from <HOST>

# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
DELIM

cat > /etc/fail2ban/filter.d/freeswitch-acl.conf  <<DELIM

[Definition]

# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named \"host\". The tag \"<HOST>\" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>[\w\-.^_]+)
# Values:  TEXT
#
failregex = \[WARNING\] sofia.c:\d+ IP <HOST> Rejected by acl "trusted"

# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
DELIM


cat > /etc/fail2ban/filter.d/rails-app.conf  <<DELIM
[Definition]
failregex = : <HOST> :
ignoreregex =
DELIM

printf "\n--------- fail2ban done ---------\n" >> ~/voxbox-install.log
