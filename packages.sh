SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh

printf "\n--------- packages starting... ---------\n" >> ~/voxbox-install.log

apt-get update && add-apt-repository -y ppa:chris-lea/node.js && apt-add-repository -y ppa:chris-lea/redis-server && apt-get update
apt-get install -y nullmailer freetds-bin sysvbanner automake redis-server nodejs openjdk-7-jre libpng-dev sharutils git build-essential libjson0-dev libxml2-dev pkg-config gcc make libfuse-dev libcurl4-openssl-dev libssl-dev libjson-c-dev tofrodos libpcap-dev libpcre3-dev libedit-dev speex libspeexdsp-dev libspeex-dev libpcre-ocaml-dev libapr1 libaprutil1 libc6-dev libltdl-dev libreadline6 libreadline6-dev libsqlite3-0 libsqlite3-dev libtool libxslt-dev libxslt1-dev libyaml-dev libncurses5 libncurses5-dev libjpeg-dev zlib1g-dev libssl-dev libexpat1-dev libtiff4-dev libx11-dev libssl-dev libzrtpcpp-dev libasound2-dev libogg-dev libvorbis-dev libperl-dev libgdbm-dev libdb-dev libmysqlclient-dev libxrender-dev libqt4-dev unixodbc-dev python-dev uuid-dev ncurses-dev zlib1g-dev bison openssl sqlite3 zlib1g gawk curl git-core subversion tshark lksctp-tools python-software-properties python g++ zip rar unrar sox lame tree python-setuptools rdoc tofrodos colordiff iftop dnstop vnstat ethtool atop monit fail2ban mysql-client nmap software-properties-common devscripts libjpeg-dev libtiff-dev gnutls-bin libmyodbc libapr1-dev libpcre3-dev lua5.1 mmv autoconf xclip wkhtmltopdf xvfb qt4-dev-tools imagemagick mc libjpeg-dev linux-headers-$(uname-r)
update-alternatives --set awk /usr/bin/gawk && ln -s /usr/bin/identify /usr/local/bin/
apt-get -y autoremove && apt-get autoclean 

export GIT_BUILD_SRC=https://git.voxbox.io/matteo/

cd /src 
wget ${GIT_BUILD_SRC}voxbox-sipgrep/repository/archive.tar.gz
tar -xvf archive.tar.gz && rm archive.tar.gz && cd voxbox-sipgrep.git/ 
chmod +x build.sh && ./build.sh && ./configure && make && make install
ln -s /src/voxbox-sipgrep.git/sipgrep /usr/bin/
apt-get -y autoremove && apt-get autoclean     

wget ${GIT_BUILD_SRC}voxbox-sipp/repository/archive.tar.gz
tar -xvf archive.tar.gz && rm archive.tar.gz && cd voxbox-sipp.git/ 
autoreconf -ivf
./configure --with-pcap
make && make install

printf "\n--------- packages done ---------\n" >> ~/voxbox-install.log
