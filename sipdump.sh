SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. $SCRIPTPATH/variables.sh
printf "\n--------- pcapsipdump starting... ---------\n" >> ~/voxbox-install.log  

export GIT_BUILD_SRC=http://git.voxbox.io/matteo/

cd /src && wget ${GIT_BUILD_SRC}voxbox-sipdump/repository/archive.tar.gz
tar -xvf archive.tar.gz && rm archive.tar.gz && cd /src/voxbox-sipdump.git 
cc pcapsipdump.cpp calltable.cpp -lpcap -lstdc++ -o pcapsipdump
cd /usr/bin/ && ln -fs /src/voxbox-sipdump.git/pcapsipdump

cd /etc/init.d/ && wget ${GIT_BUILD_SRC}voxbox-config-files/raw/master/etc/init.d/sipdump
chmod +x sipdump && update-rc.d -f sipdump defaults && update-rc.d -f sipdump enable && /etc/init.d/sipdump start
printf "\n--------- pcapsipdump end ---------\n" >> ~/voxbox-install.log 

